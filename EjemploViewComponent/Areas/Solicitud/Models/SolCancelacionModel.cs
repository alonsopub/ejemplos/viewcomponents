﻿namespace EjemploViewComponent.Areas.Solicitud.Models;

public class SolCancelacionModel
{
    public string modo { set; get; }
    public int idSol { set; get; }
    public DateTime? FCancelacion { set; get; }
    public string? motivoCanc { set; get; }
}
