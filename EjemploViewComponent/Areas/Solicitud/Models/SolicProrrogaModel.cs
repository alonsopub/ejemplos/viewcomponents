﻿namespace EjemploViewComponent.Areas.Solicitud.Models;


public class SolicProrrogaModel
{
    public int idSol { set; get; }
    public string? txProrroga { set; get; }
}
