﻿using Microsoft.AspNetCore.Mvc;
using TipoSolicitudViewComponents.Models;

namespace EjemploViewComponent.Areas.Solicitud.ViewComponents;

[Area("Solicitud")]
[ViewComponent(Name = "cancelacion")]
public class CancelacionViewComponent : ViewComponent
{
    public IViewComponentResult Invoke(SolicitudModel model)
    {
        SolCancelacionModel modelCanc = new SolCancelacionModel();
        modelCanc.idSol = model.idSol;
        modelCanc.motivoCanc = "mejores opciones";
        modelCanc.modo = model.modo;
        return View(modelCanc);
    }
}
