﻿using Microsoft.AspNetCore.Mvc;
using TipoSolicitudViewComponents.Models;

namespace EjemploViewComponent.Areas.Solicitud.ViewComponents;


[Area("Solicitud")]
[ViewComponent(Name = "prorroga")]
public class ProrrogaViewComponent : ViewComponent
{
    public IViewComponentResult Invoke(SolicitudModel model)
    {
        SolicProrrogaModel modelProrroga = new SolicProrrogaModel();
        modelProrroga.idSol = model.idSol;
        return View(modelProrroga);
    }
}
