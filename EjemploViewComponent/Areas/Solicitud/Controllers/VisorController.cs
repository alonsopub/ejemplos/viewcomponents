﻿using Microsoft.AspNetCore.Mvc;
using TipoSolicitudViewComponents.Models;

namespace EjemploViewComponent.Areas.Solicitud.Controllers
{
    [Area("Solicitud")]
    public class VisorController : Controller
    {
        public IActionResult Index(int id)
        {

            SolicitudModel model = new SolicitudModel();

            if(id==405)
            {
                model.idSol = id;
                model.tipoSol = "Cancelación de Póliza";
                model.vista = "tsCancelacion"; // "cancelacion";
                model.xmlSol = "<xml><solCancelacion><Datos FCancelacion='20221115' /></solCancelacion>";
            }
            else if(id==501)
            {
                model.idSol = id;
                model.tipoSol = "Cambio de Prorroga";
                model.vista = "tsProrroga"; //"prorroga";
                model.xmlSol = "<xml><solProrroga><Datos txtProrroga='Actualizalar la inf..' /></solProrroga>";
            }
            
            return View(model);
        }

        [HttpPost]
        public IActionResult ActualizaProrroga(SolicProrrogaModel model)
        {
            return View(model);
        }
    }
}
