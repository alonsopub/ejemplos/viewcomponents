﻿using Microsoft.AspNetCore.Mvc;
using TipoSolicitudViewComponents.Models;

namespace TipoSolicitudViewComponents.Components;

[ViewComponent(Name = "tsProrroga")]
public class TSProrrogaViewComponent : ViewComponent
{
    public IViewComponentResult Invoke(SolicitudModel model)
    {
        SolicProrrogaModel modelProrroga = new SolicProrrogaModel();
        modelProrroga.idSol = model.idSol;
        return View(modelProrroga);
    }
}