﻿using Microsoft.AspNetCore.Mvc;
using TipoSolicitudViewComponents.Models;

namespace TipoSolicitudViewComponents.Components;

[ViewComponent(Name = "tsCancelacion")]
public class TSCancelacionViewComponent : ViewComponent
{
    public IViewComponentResult Invoke(SolicitudModel model)
    {
        SolCancelacionModel modelCanc = new SolCancelacionModel();
        modelCanc.idSol = model.idSol;
        modelCanc.motivoCanc = "mejores opciones";
        modelCanc.modo = model.modo;
        return View(modelCanc);
    }
}