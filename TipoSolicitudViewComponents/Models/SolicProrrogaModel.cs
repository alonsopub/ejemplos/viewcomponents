﻿

namespace TipoSolicitudViewComponents.Models;

public class SolicProrrogaModel
{
    public int idSol { set; get; }
    public string? txProrroga { set; get; }
}