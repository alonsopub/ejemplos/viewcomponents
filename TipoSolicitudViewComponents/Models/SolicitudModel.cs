﻿namespace TipoSolicitudViewComponents.Models;


public class SolicitudModel
{
    public string modo { set; get; }
    public int idSol { set; get; }
    public string tipoSol { set; get; }
    public string vista { set; get; }
    public string xmlSol { set; get; }
}